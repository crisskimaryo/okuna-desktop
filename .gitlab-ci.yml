image: rust:stretch

stages:
  - lint
  - build
  - release
  - package
  - deploy

# Lint jobs

rustfmt:
  stage: lint
  script:
    - rustup component add rustfmt
    - cargo fmt -- --check

# Build jobs

## Flutter app
flutter-app:
  image: cirrusci/flutter:stable
  stage: build
  variables:
    GIT_SUBMODULE_STRATEGY: "recursive"
  before_script:
    # Upgrade to specific flutter commmit (v1.10.15-pre.252)
    - dir="$(pwd)"
    - cd ~/sdks/flutter
    - git fetch origin
    - git checkout 903ebd714d2209532012349d814161865bfbb710
    - flutter doctor -v
    - cd "$dir"
  script:
    - cd okuna-app
    - echo "{\"API_URL\":\"https://api.openbook.social/\",\"INTERCOM_IOS_KEY\":\"\",\"INTERCOM_ANDROID_KEY\":\"\",\"INTERCOM_APP_ID\":\"\",\"SENTRY_DSN\":\"https://dcb6ec1db9dc4ad3b64a795b31819688@sentry.openbook.social/4\",\"OPENBOOK_SOCIAL_API_URL\":\"https://api.us.openbook.social/\",\"LINK_PREVIEWS_TRUSTED_PROXY_URL\":\"https://contentproxy.okuna.io/\"}" > .env.json
    - patch -uNp1 -i ../releasing/binaries/emoji-fix.patch
    - patch -uNp1 -i ../releasing/binaries/keyboard-fix.patch
    - patch -uNp1 -i ../releasing/binaries/about.patch
    - mv ../releasing/binaries/TwemojiAndroid.ttf assets/fonts/Twemoji.ttf
    - flutter build bundle
    - cd ..
    - if [ -d build ]; then rm -rf build; fi
    - mkdir -p build
    - cp -r okuna-app/build/flutter_assets build/
    - cp releasing/binaries/icudtl.dat build/
  artifacts:
    paths:
      - build

## Rust wrapper
x86_64-unknown-linux-gnu:
  stage: build
  variables:
    TARGET: "x86_64-unknown-linux-gnu"
    FLUTTER_ENGINE_VERSION: "8d6b74aee6fb81e4cb8c4cce43a4755f89f6d9d7"
    SHELL: /bin/sh
  before_script:
    - mkdir -p .cargo
    - echo -e "[target.x86_64-unknown-linux-gnu]\\nrustflags = [\"-C\",\"link-args=-Wl,-rpath,.\"]" > .cargo/config
  script:
    - apt-get update
    - apt-get -y install cmake libxrandr-dev libxinerama-dev libxcursor-dev libgl1-mesa-dev libxxf86vm-dev libxi-dev clang-7 llvm-7-dev libclang-7-dev libgtk-3-dev autoconf2.13 yasm libx264-dev libgnutls28-dev
    - rustup target add $TARGET
    - cargo install cargo-sweep
    - cargo sweep -s
    - touch build.rs
    - cargo build --release --target=$TARGET --features=ffmpeg-linux
    - cargo sweep -f
  after_script:
    - mkdir -p build/x86_64-linux
    - cp target/$TARGET/release/okuna-desktop build/x86_64-linux/
    - cp target/$TARGET/release/libflutter_engine.so build/x86_64-linux/
    - cp target/$TARGET/release/ffmpeg build/x86_64-linux/
    - strip -s build/x86_64-linux/libflutter_engine.so
    - strip -s build/x86_64-linux/okuna-desktop
    - strip -s build/x86_64-linux/ffmpeg
    - cp releasing/os/unix/okuna-desktop.sh build/x86_64-linux/
    - cp releasing/os/linux/okuna-desktop.desktop build/x86_64-linux/
  artifacts:
    paths:
      - build
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - target/

x86_64-pc-windows-msvc:
  stage: build
  tags:
    - windows
  variables:
    MOZTOOLS_PATH: C:\mozilla-build\msys\bin;C:\mozilla-build\bin
    MSYS_PATH: C:\msys64\usr\bin;C:\mozilla-build\bin
    LIBCLANG_PATH: C:\Program Files\LLVM\lib
    AUTOCONF: C:\mozilla-build\msys\local\bin\autoconf-2.13
    NATIVE_WIN32_PYTHON: C:\mozilla-build\python\python2.7.exe
    EXTRA_LIB_PATHS: C:\x264\install\lib
    EXTRA_INCLUDE_PATHS: C:\x264\install\include
    CFLAGS: -DWIN32_LEAN_AND_MEAN
    TARGET: x86_64-pc-windows-msvc
    FLUTTER_ENGINE_VERSION: "8d6b74aee6fb81e4cb8c4cce43a4755f89f6d9d7"
  script:
    - releasing\ci\msvc_vars.ps1
    - $env:Path = "$env:MSYS_PATH;$env:Path"
    - $env:LIB += ";$env:EXTRA_LIB_PATHS"
    - $env:INCLUDE += ";$env:EXTRA_INCLUDE_PATHS"
    - (get-childitem build.rs).lastwritetime = get-date
    - cargo build --release --target=$TARGET --features=ffmpeg-windows
  after_script:
    - releasing\ci\msvc_vars.ps1
    - new-item -itemtype directory -name build/x86_64-windows
    - copy-item target/$TARGET/release/okuna-desktop.exe -destination build/x86_64-windows/
    - copy-item target/$TARGET/release/flutter_engine.dll -destination build/x86_64-windows/
    - copy-item target/$TARGET/release/ffmpeg.exe build/x86_64-windows/
    - copy-item "$Env:VCToolsRedistDir\vc_redist.x64.exe" -destination build/x86_64-windows/
    - copy-item target/$TARGET/release/build/mozjs_sys-*/out/dist/bin/nspr4.dll build/x86_64-windows/
    - new-item -itemtype directory -name build/x86_64-windows/flutter_assets/assets/fonts
    - copy-item releasing/binaries/TwemojiMozilla.ttf -destination build/x86_64-windows/flutter_assets/assets/fonts/Twemoji.ttf
  artifacts:
    paths:
      - build
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - target/

x86_64-apple-darwin:
  stage: build
  variables:
    TARGET: "x86_64-apple-darwin"
    FLUTTER_ENGINE_VERSION: "8d6b74aee6fb81e4cb8c4cce43a4755f89f6d9d7"
    CONTENT_DIR: "build/x86_64-macos/OkunaDesktop.app/Contents"
    EXECUTABLE_DIR: "${CONTENT_DIR}/MacOS"
    FRAMEWORK_DIR: "${CONTENT_DIR}/Frameworks"
    RESOURCE_DIR: "${CONTENT_DIR}/Resources"
    LIB_DIR: "${CONTENT_DIR}/libs"
    LIB_SEARCH_PATH: "@executable_path/../libs"
  tags:
    - macos
  before_script:
    - mkdir -p .cargo
    - echo -e "[target.x86_64-apple-darwin]\\nrustflags = [\"-C\",\"link-args=-Wl,-rpath,@executable_path/../Frameworks/ -mmacosx-version-min=10.7\",\"-L\",\"/usr/local/opt/openssl/lib\"]" > .cargo/config
  script:
    - touch build.rs
    - cargo build --release --target=$TARGET --features=ffmpeg-macos
    - releasing/os/macos/make_icns.sh
  after_script:
    - mkdir -p "${EXECUTABLE_DIR}"
    - mkdir -p "${FRAMEWORK_DIR}"
    - mkdir -p "${RESOURCE_DIR}"
    - cp target/$TARGET/release/okuna-desktop "${EXECUTABLE_DIR}/"
    - cp target/$TARGET/release/ffmpeg "${EXECUTABLE_DIR}/"
    - cp releasing/os/unix/okuna-desktop.sh "${EXECUTABLE_DIR}/okuna-desktop.command"
    - cp -r libs/${FLUTTER_ENGINE_VERSION}/FlutterEmbedder.framework "${FRAMEWORK_DIR}/"
    - strip "${EXECUTABLE_DIR}/ffmpeg"
    - dylibbundler -od -b -x "${EXECUTABLE_DIR}/okuna-desktop" -d "${LIB_DIR}/" -p "${LIB_SEARCH_PATH}/"
    - dylibbundler -of -b -x "${EXECUTABLE_DIR}/ffmpeg" -d "${LIB_DIR}/" -p "${LIB_SEARCH_PATH}/"
    - cp assets/OkunaDesktop.icns "${RESOURCE_DIR}/"
    - cp releasing/os/macos/Info.plist "${CONTENT_DIR}/"
    - sed -i ".bak" -e "s/\$VERSION/$(cat version)/g" "${CONTENT_DIR}/Info.plist"
  artifacts:
    paths:
      - build
  cache:
    key: "$CI_JOB_NAME"
    paths:
      - target/

# Release jobs

.release-template:
  stage: release
  image: alpine
  variables:
    RESOURCE_DIR: okuna-desktop
  before_script:
    - mkdir -p "${RESOURCE_DIR}"
    - cp -r build/flutter_assets "${RESOURCE_DIR}/"
    - cp build/icudtl.dat "${RESOURCE_DIR}/"
    - cp -r assets/ "${RESOURCE_DIR}/"
  artifacts:
    paths:
      - ${RESOURCE_DIR}

release-linux:
  extends: .release-template
  dependencies:
    - x86_64-unknown-linux-gnu
    - flutter-app
  script:
    - cp -r build/x86_64-linux/* "${RESOURCE_DIR}/"

release-windows:
  extends: .release-template
  dependencies:
    - x86_64-pc-windows-msvc
    - flutter-app
  script:
    - cp -r build/x86_64-windows/* "${RESOURCE_DIR}/"

release-macos:
  extends: .release-template
  variables:
    RESOURCE_DIR: OkunaDesktop.app/Contents/Resources
  dependencies:
    - x86_64-apple-darwin
    - flutter-app
  script:
    - cp -r build/x86_64-macos/OkunaDesktop.app/* OkunaDesktop.app/
  artifacts:
    paths:
      - OkunaDesktop.app

# Package jobs

.package-template:
  stage: package
  dependencies:
    - release-linux
  script:
    - useradd -Md $(pwd) user
    - echo -e "user ALL=(ALL) ALL\\nDefaults:user !authenticate" >> /etc/sudoers
    - chmod -R o+rw .
    - su -c releasing/packages/$distro/package.sh user
    - mkdir packages

package-arch:
  image: archlinux/base
  extends: .package-template
  variables:
    distro: arch
  before_script:
    - pacman -Syu --noconfirm
    - pacman -S --noconfirm --needed --asdeps jq base-devel git
    - echo "INTEGRITY_CHECK=('sha256')" > ~/.makepkg.conf
  after_script:
    - cp releasing/packages/arch/{PKGBUILD,.SRCINFO,*.pkg.tar.xz} packages
  artifacts:
    paths:
      - packages

package-appimage:
  image: debian:stretch
  extends: .package-template
  variables:
    distro: appimage
  before_script:
    - apt-get update
    - apt-get -y install git jq curl binutils fuse file patchelf libx264-148 libgnutls30 libsqlite3-0
  after_script:
    - cp releasing/packages/$distro/Okuna*.AppImage packages
  artifacts:
    paths:
      - packages

package-debian:
  image: debian:stretch
  extends: .package-template
  variables:
    distro: debian
  before_script:
    - apt-get update
    - apt-get -y install dh-make jq git build-essential unzip wget curl devscripts libx264-148 libgnutls30
  after_script:
    - cp releasing/packages/debian/*.deb packages
  artifacts:
    paths:
      - packages

# Deploy jobs

deploy-gitlab-release:
  image: alpine
  stage: deploy
  dependencies: []
  script:
    - apk --no-cache add curl jq
    - releasing/ci/create_gitlab_release.sh
  only:
    - tags
