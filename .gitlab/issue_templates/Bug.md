#Summary

(Concise summary of the issue)

# Reproducing

(How can this issue be reproduced / When does it occur?)

# Actual behaviour

(Description of what you see)

# Expected behaviour

(Description of what you expected to happen)

# Logs and screenshots

(Please attach a log file and optionally a screenshot. Please either attach the log as a file or use code blocks (```) to format text. See [here](https://gitlab.com/999eagle/openbook-desktop#issues) for the path to the log file)
