use std::{
    collections::VecDeque,
    convert::TryInto,
    ops::{Add, AddAssign, Sub},
    path::Path,
    sync::{
        mpsc::{channel, Sender},
        Arc, Mutex, RwLock, Weak,
    },
    thread::{self, JoinHandle},
    time::{Duration, Instant},
};

use ffmpeg::{
    codec::{
        decoder::{Audio as AudioDecoder, Decoder as FFmpegDecoder, Video as VideoDecoder},
        packet::Packet,
    },
    format::{context::Input, stream::Stream},
    media::Type,
    software::scaling::{self, Context as FFmpegScalingContext},
    util::{
        format::pixel::Pixel as PixelFormat,
        frame::{Audio as AudioFrame, Video as VideoFrame},
        mathematics::rescale::TIME_BASE,
    },
};
use flutter_engine::{texture_registry::ExternalTexture, RuntimeData};
use log::{debug, error, warn};

use lazy_static::lazy_static;

const TIME_BASE_MULTIPLIER: f64 = TIME_BASE.0 as f64 / TIME_BASE.1 as f64;
const SEEK_TIMEOUT: u64 = 200;

const VIDEO_PACKET_QUEUE_MAX: usize = 1024;
const AUDIO_PACKET_QUEUE_MAX: usize = 512;

const NO_PACKET_SLEEP: u64 = 10;
const QUEUE_FULL_SLEEP: u64 = 50;
const NO_TEXTURE_SLEEP: u64 = 50;
const FRAME_TIMEOUT_MAX: u64 = 5;
const PAUSE_SLEEP: u64 = 100;

lazy_static! {
    static ref FRAME_SLEEP_EPSILON: Duration = Duration::from_millis(1);
}

pub struct FFmpegPlayer {
    uri: String,
    texture: Arc<ExternalTexture>,
    state: Option<Arc<Mutex<VideoState>>>,
    threads: Vec<JoinHandle<()>>,
    time: Option<Arc<RwLock<TimeData>>>,
}

pub struct InitResult {
    pub duration: i64,
    pub size: (u32, u32),
}

struct VideoState {
    input: Input,
    loop_count: u32,
    video: Arc<Mutex<VideoStreamData>>,
    audio: Option<Arc<Mutex<StreamData>>>,
    time: Arc<RwLock<TimeData>>,
    seek_data: Option<Seek>,
}

struct Seek {
    target: i64,
    timeout: Instant,
}

enum Decoder {
    Video(VideoDecoder),
    Audio(AudioDecoder),
}

struct VideoStreamData {
    stream: StreamData,
    size_sender: Option<Sender<(u32, u32)>>,
    scaler: Option<ScalingContext>,
    texture: Arc<ExternalTexture>,
    source_frame: Option<(VideoFrame, u32)>,
    texture_frame: Option<(VideoFrame, u32)>,
    set_start_time: Option<Duration>,
}

struct ScalingContext {
    context: FFmpegScalingContext,
}

unsafe impl Send for ScalingContext {}

enum PacketData {
    Packet(Packet, u32),
    Flush,
}

struct StreamData {
    stream_index: usize,
    decoder: Decoder,
    time_base: f64,
    duration: i64,
    time: Arc<RwLock<TimeData>>,
    packet_queue: VecDeque<PacketData>,
}

struct TimeData {
    start_time: Instant,
    paused: Option<Instant>,
    looping: bool,
    duration: i64,
}

impl TimeData {
    fn new() -> Self {
        let now = Instant::now();
        Self {
            start_time: now,
            paused: Some(now),
            looping: false,
            duration: 0,
        }
    }
}

impl Decoder {
    fn new_video(d: FFmpegDecoder) -> Self {
        Decoder::Video(d.video().unwrap())
    }

    fn as_video(&mut self) -> &mut VideoDecoder {
        if let Decoder::Video(d) = self {
            d
        } else {
            panic!("wrong type")
        }
    }

    fn new_audio(d: FFmpegDecoder) -> Self {
        Decoder::Audio(d.audio().unwrap())
    }

    fn as_audio(&mut self) -> &mut AudioDecoder {
        if let Decoder::Audio(d) = self {
            d
        } else {
            panic!("wrong type")
        }
    }
}

impl StreamData {
    fn new<D: FnOnce(FFmpegDecoder) -> Decoder>(
        stream: &Stream,
        decoder_fn: D,
        time: Arc<RwLock<TimeData>>,
    ) -> Self {
        let time_base = stream.time_base();
        let time_base = time_base.numerator() as f64 / time_base.denominator() as f64;
        // calculate duration in ms
        let duration = stream.duration() as f64 * time_base;
        let duration = (duration * 1000_f64) as i64;

        Self {
            stream_index: stream.index(),
            decoder: decoder_fn(stream.codec().decoder()),
            time_base,
            duration,
            time,
            packet_queue: VecDeque::new(),
        }
    }
}

impl VideoStreamData {
    fn new(stream: StreamData, texture: Arc<ExternalTexture>) -> Self {
        Self {
            stream,
            size_sender: None,
            scaler: None,
            texture,
            source_frame: None,
            texture_frame: None,
            set_start_time: None,
        }
    }
}

impl FFmpegPlayer {
    pub fn new(uri: String, texture: Arc<ExternalTexture>) -> Self {
        debug!("creating ffmpeg player");

        Self {
            uri,
            texture,
            state: None,
            threads: Vec::new(),
            time: None,
        }
    }

    pub fn init(&mut self, rt: RuntimeData) -> InitResult {
        debug!("initialising player");

        let time = Arc::new(RwLock::new(TimeData::new()));
        let input = ffmpeg::format::input(&Path::new(&self.uri)).unwrap();
        let video = Arc::new(Mutex::new(VideoStreamData::new(
            StreamData::new(
                &input.streams().best(Type::Video).unwrap(),
                Decoder::new_video,
                Arc::clone(&time),
            ),
            Arc::clone(&self.texture),
        )));
        let rx = {
            let (tx, rx) = channel();
            let mut video = video.lock().unwrap();
            video.size_sender = Some(tx);
            rx
        };
        let duration = video.lock().unwrap().stream.duration;
        let weak_video = Arc::downgrade(&video);
        let audio = input.streams().best(Type::Audio).map(|s| {
            Arc::new(Mutex::new(StreamData::new(
                &s,
                Decoder::new_audio,
                Arc::clone(&time),
            )))
        });
        let weak_audio = audio.as_ref().map(Arc::downgrade);
        let state = Arc::new(Mutex::new(VideoState {
            time: Arc::clone(&time),
            loop_count: 0,
            input,
            video,
            audio,
            seek_data: None,
        }));
        let weak_state = Arc::downgrade(&state);

        {
            let mut time = time.write().unwrap();
            time.duration = duration;
        }

        self.time.replace(time);

        let own_rt = rt;
        let rt = own_rt.clone();
        self.threads.push(thread::spawn(|| {
            run_player_thread(weak_state, "queue".into(), enqueue_next_packet, rt)
        }));
        let rt = own_rt.clone();
        self.threads.push(thread::spawn(move || {
            run_player_thread(weak_video, "video player".into(), play_video, rt)
        }));
        if let Some(weak_audio) = weak_audio {
            let rt = own_rt.clone();
            self.threads.push(thread::spawn(|| {
                run_player_thread(weak_audio, "audio player".into(), play_audio, rt)
            }));
        }

        // wait until the first frame has been decoded and we know the video size
        let size = rx.recv().unwrap();

        self.state.replace(state);

        InitResult { duration, size }
    }

    pub fn pause(&self) {
        let time = if let Some(time) = self.time.as_ref() {
            time
        } else {
            return;
        };

        let mut time = time.write().unwrap();
        if time.paused.is_none() {
            time.paused = Some(Instant::now());
        }
    }

    pub fn play(&self) {
        let time = if let Some(time) = self.time.as_ref() {
            time
        } else {
            return;
        };

        let mut time = time.write().unwrap();
        if let Some(paused) = time.paused.take() {
            let now = Instant::now();
            time.start_time.add_assign(now.duration_since(paused));
        }
    }

    pub fn position(&self) -> i64 {
        let time = if let Some(time) = self.time.as_ref() {
            time
        } else {
            return 0;
        };

        let time = time.read().unwrap();
        let now = Instant::now();
        if now <= time.start_time {
            0
        } else {
            now.duration_since(time.start_time).as_millis() as i64 % time.duration
        }
    }

    pub fn set_looping(&self, looping: bool) {
        let time = if let Some(time) = self.time.as_ref() {
            time
        } else {
            return;
        };

        let mut time = time.write().unwrap();
        time.looping = looping;
    }

    pub fn seek_to(&self, target: i64) {
        let state = if let Some(state) = self.state.as_ref() {
            state
        } else {
            return;
        };
        let mut state = state.lock().unwrap();
        let timeout = Instant::now().add(Duration::from_millis(SEEK_TIMEOUT));
        if let Some(seek) = &mut state.seek_data {
            seek.target = target;
            seek.timeout = timeout;
        } else {
            state.seek_data.replace(Seek { target, timeout });
        }
    }
}

impl Drop for FFmpegPlayer {
    fn drop(&mut self) {
        // drop the state to make sure that threads exit
        self.state.take();
        while let Some(t) = self.threads.pop() {
            if let Err(err) = t.join() {
                warn!("thread exited with error: {:?}", err);
            }
        }
    }
}

enum LoopState {
    Running,
    Sleep(u64),
    Exit,
}

fn run_player_thread<F, T>(state: Weak<Mutex<T>>, description: String, f: F, rt: RuntimeData)
where
    F: Fn(&mut T, &RuntimeData) -> LoopState,
{
    debug!(
        "video thread '{}' ({:?}) starting",
        description,
        thread::current().id()
    );
    // exit this loop as soon as the state itself has been lost
    while let Some(state) = state.upgrade() {
        // run this in a block to drop the mutex guard before sleeping
        let loop_state = {
            let mut state = state.lock().unwrap();
            f(&mut *state, &rt)
        };

        match loop_state {
            LoopState::Exit => break,
            LoopState::Sleep(millis) => thread::sleep(Duration::from_millis(millis)),
            LoopState::Running => (),
        }
    }
    debug!(
        "video thread '{}' ({:?}) exiting",
        description,
        thread::current().id()
    );
}

fn enqueue_next_packet(state: &mut VideoState, _: &RuntimeData) -> LoopState {
    let mut video = state.video.lock().unwrap();
    let mut audio = state.audio.as_ref().map(|a| a.lock().unwrap());

    // check for video seek
    if let Some(seek) = &mut state.seek_data {
        if Instant::now() >= seek.timeout {
            // okay, timeout happened so now we seek
            let mut time = state.time.write().unwrap();
            let duration = Duration::from_millis(seek.target as u64);
            let target = (seek.target as f64 / 1000_f64 / TIME_BASE_MULTIPLIER) as i64; // ms to s to time_base
            let offset = (0.1_f64 / TIME_BASE_MULTIPLIER) as i64;
            let _ = state.input.seek(target, target - offset..target + offset);

            state.loop_count = 0;
            video.texture_frame.take();
            video.source_frame.take();
            video.stream.packet_queue.clear();
            video.stream.packet_queue.push_back(PacketData::Flush);
            video.set_start_time.replace(duration);
            if let Some(audio) = audio.as_mut() {
                audio.packet_queue.clear();
                audio.packet_queue.push_back(PacketData::Flush);
            }

            state.seek_data.take();
            time.start_time = Instant::now().sub(duration);
        }
    }

    // sleep if the queues are full
    if video.stream.packet_queue.len() >= VIDEO_PACKET_QUEUE_MAX {
        return LoopState::Sleep(QUEUE_FULL_SLEEP);
    }
    if let Some(audio) = &audio {
        if audio.packet_queue.len() >= AUDIO_PACKET_QUEUE_MAX {
            return LoopState::Sleep(QUEUE_FULL_SLEEP);
        }
    }

    // unlock video and audio while getting next packet
    drop(video);
    drop(audio);

    // read input packets and queue them to the correct queue
    let packet = state.input.packets().next();
    let mut video = state.video.lock().unwrap();
    let mut audio = state.audio.as_ref().map(|a| a.lock().unwrap());

    if let Some((stream, packet)) = packet {
        let idx = stream.index();
        if idx == video.stream.stream_index {
            video
                .stream
                .packet_queue
                .push_back(PacketData::Packet(packet, state.loop_count));
        } else if let Some(audio) = &mut audio {
            if idx == audio.stream_index {
                audio
                    .packet_queue
                    .push_back(PacketData::Packet(packet, state.loop_count));
            }
        }
    } else {
        // eof reached
        let time = state.time.read().unwrap();
        if !time.looping {
            return LoopState::Sleep(PAUSE_SLEEP);
        }
        // looping -> seek to beginning
        let _ = state.input.seek(0, 0..i64::max_value());
        video.stream.packet_queue.push_back(PacketData::Flush);
        if let Some(audio) = &mut audio {
            audio.packet_queue.push_back(PacketData::Flush);
        }
        state.loop_count += 1;
    }

    LoopState::Running
}

fn get_source_frame(video: &mut VideoStreamData) -> Result<(VideoFrame, u32), LoopState> {
    let (packet, loop_count) = if let Some(packet) = video.stream.packet_queue.pop_front() {
        match packet {
            PacketData::Packet(p, l) => (p, l),
            PacketData::Flush => {
                video.stream.decoder.as_video().flush();
                return get_source_frame(video);
            }
        }
    } else {
        return Err(LoopState::Sleep(NO_PACKET_SLEEP));
    };
    let decoder = video.stream.decoder.as_video();
    let mut frame = VideoFrame::empty();
    match decoder.decode(&packet, &mut frame) {
        Err(err) => {
            error!("failed to decode video frame: {}", err);
            Err(LoopState::Exit)
        }
        Ok(_) => {
            if frame.format() == PixelFormat::None {
                get_source_frame(video)
            } else {
                Ok((frame, loop_count))
            }
        }
    }
}

fn scale_source_frame(
    video: &mut VideoStreamData,
    source_frame: &VideoFrame,
) -> Result<VideoFrame, LoopState> {
    let size = if let Some(size) = video.texture.size() {
        size
    } else {
        // we don't know the target size yet
        return Err(LoopState::Sleep(NO_TEXTURE_SLEEP));
    };

    if let Some(scaler) = video.scaler.as_ref() {
        if scaler.context.input().width != source_frame.width()
            || scaler.context.input().height != source_frame.height()
            || scaler.context.input().format != source_frame.format()
            || scaler.context.output().width != size.0
            || scaler.context.output().height != size.1
        {
            video.scaler.take();
        }
    }
    let scaler = if let Some(scaler) = video.scaler.as_mut() {
        scaler
    } else {
        video.scaler.replace(ScalingContext {
            context: FFmpegScalingContext::get(
                source_frame.format(),
                source_frame.width(),
                source_frame.height(),
                PixelFormat::RGBA,
                size.0,
                size.1,
                scaling::flag::BILINEAR,
            )
            .unwrap(),
        });
        video.scaler.as_mut().unwrap()
    };
    let mut scaled_frame = VideoFrame::empty();
    scaler.context.run(source_frame, &mut scaled_frame).unwrap();
    scaled_frame.set_pts(source_frame.pts());
    Ok(scaled_frame)
}

fn play_video(video: &mut VideoStreamData, rt: &RuntimeData) -> LoopState {
    let (rgb_frame, loop_count) = if let Some(frame) = video.texture_frame.take() {
        frame
    } else {
        // no texture frame available, get one from a source frame
        let (source_frame, loop_count) = if let Some(frame) = video.source_frame.take() {
            frame
        } else {
            // no source frame available either, decode a new one
            match get_source_frame(video) {
                Ok(frame) => frame,
                Err(state) => return state,
            }
        };
        // store size for external access
        if let Some(tx) = video.size_sender.take() {
            tx.send((source_frame.width(), source_frame.height()))
                .unwrap();
        }
        // scale frame to texture size
        match scale_source_frame(video, &source_frame) {
            Ok(frame) => (frame, loop_count),
            Err(state) => {
                video.source_frame.replace((source_frame, loop_count));
                return state;
            }
        }
    };

    let start_time = {
        let time = video.stream.time.read().unwrap();
        if time.paused.is_some() {
            video.texture_frame.replace((rgb_frame, loop_count));
            return LoopState::Sleep(PAUSE_SLEEP);
        }
        if let Some(duration) = video.set_start_time.take() {
            drop(time);
            let mut time = video.stream.time.write().unwrap();
            time.start_time = Instant::now().sub(duration);
            time.start_time
        } else {
            time.start_time
        }
    };

    // calculate correct display time for frame
    let display_time = rgb_frame.pts().unwrap() as f64 * video.stream.time_base;
    let display_time =
        (display_time * 1000_f64) as u64 + (video.stream.duration as u64 * loop_count as u64);
    let display_time = start_time.add(Duration::from_millis(display_time));
    let now = Instant::now();
    if display_time > now {
        let diff = display_time.duration_since(now);
        if diff > *FRAME_SLEEP_EPSILON {
            video.texture_frame.replace((rgb_frame, loop_count));
            return LoopState::Sleep((diff.as_millis() as u64).max(FRAME_TIMEOUT_MAX));
        }
    }

    // display frame in render thread
    let texture = Arc::clone(&video.texture);
    let thread_rt = rt.clone();
    let send_result = rt.post_to_render_thread(move |_| unsafe {
        let texture_name = texture.gl_texture().unwrap();
        let stride = rgb_frame.stride(0) / 4;
        gl::ActiveTexture(gl::TEXTURE0);
        gl::BindTexture(gl::TEXTURE_2D, texture_name);
        gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
        gl::PixelStorei(gl::UNPACK_ROW_LENGTH, stride.try_into().unwrap());
        gl::TexSubImage2D(
            gl::TEXTURE_2D,
            0,                                      // mipmap level
            0,                                      // x offset
            0,                                      // y offset
            rgb_frame.width().try_into().unwrap(),  // width
            rgb_frame.height().try_into().unwrap(), // height
            gl::RGBA,                               // format of the pixel data
            gl::UNSIGNED_BYTE,                      // data type of the pixel data
            rgb_frame.data(0).as_ptr() as *const _, // pixel data
        );
        let texture = Arc::clone(&texture);
        let _ = thread_rt.with_window(move |_| {
            texture.mark_frame_available();
        });
    });
    if send_result.is_err() {
        return LoopState::Exit;
    }

    LoopState::Running
}

fn play_audio(audio: &mut StreamData, _rt: &RuntimeData) -> LoopState {
    let (packet, loop_count) = if let Some(packet) = audio.packet_queue.pop_front() {
        match packet {
            PacketData::Packet(p, l) => (p, l),
            PacketData::Flush => {
                audio.decoder.as_audio().flush();
                return LoopState::Running;
            }
        }
    } else {
        return LoopState::Sleep(NO_PACKET_SLEEP);
    };

    let _start_time = {
        let time = audio.time.read().unwrap();
        if time.paused.is_some() {
            audio
                .packet_queue
                .push_front(PacketData::Packet(packet, loop_count));
            return LoopState::Sleep(PAUSE_SLEEP);
        }
        time.start_time
    };

    let decoder = audio.decoder.as_audio();
    let mut frame = AudioFrame::empty();
    match decoder.decode(&packet, &mut frame) {
        Err(err) => {
            error!("failed to decode audio frame: {}", err);
            return LoopState::Exit;
        }
        Ok(_) => {
            // TODO: play audio frame
        }
    }

    LoopState::Running
}
