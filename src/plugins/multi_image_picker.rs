use std::{path::Path, sync::mpsc::SyncSender};

use flutter_engine::plugins::prelude::*;
use image::GenericImageView;
use log::{debug, error, info};

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "multi_image_picker";

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct IosOptions {
    selection_character: String,
    take_photo_icon: String,
    selection_stroke_color: String,
    background_color: String,
    selection_shadow_color: String,
    selection_text_color: String,
    selection_fill_color: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct AndroidOptions {
    start_in_all_view: String, // actually contains "true"/"false", maybe deserialize this?
    selection_limit_reached_text: String,
    action_bar_color: String,
    select_circle_stroke_color: String,
    action_bar_title_color: String,
    action_bar_title: String,
    status_bar_color: String,
    all_view_title: String,
    light_status_bar: String, // actually contains "true"/"false", maybe deserialize this?
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct PickImageArgs {
    ios_options: IosOptions,
    selected_assets: Vec<String>,
    enable_camera: bool,
    max_images: i32,
    android_options: AndroidOptions,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct RequestOriginalArgs {
    quality: i32,
    identifier: String,
}

pub struct MultiImagePickerPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    default_path: String,
    path_tx: SyncSender<String>,
}

impl Plugin for MultiImagePickerPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl MultiImagePickerPlugin {
    pub fn new(default_path: String, path_tx: SyncSender<String>) -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler {
                default_path,
                path_tx,
            })),
        }
    }
}
impl Handler {
    fn pick_images(&mut self, _args: PickImageArgs) -> Result<Value, MethodCallError> {
        let filter = Some((&["*.jpg", "*.png", "*.gif", "*.bmp"][0..], "Image files"));
        let result = tinyfiledialogs::open_file_dialog("Okuna", self.default_path.as_str(), filter);
        match result {
            Some(path) => {
                info!("Opening image from {}", path);
                let path = Path::new(path.as_str());
                if let Some(path) = path.parent() {
                    let path = path.to_str().unwrap();
                    debug!("Setting new default path to {}", path);
                    self.default_path = path.to_owned();
                    self.path_tx.send(self.default_path.clone()).unwrap();
                }
                match image::open(path.to_str().unwrap()) {
                    Ok(img) => {
                        let (width, height) = (img.width(), img.height());
                        Ok(json_value! (
                            [{
                                "width": width,
                                "height": height,
                                "identifier": path.to_string_lossy().into_owned(),
                                "name": path.file_name().unwrap().to_string_lossy().into_owned(),
                             }]
                        ))
                    }
                    Err(err) => {
                        error!(
                            "Failed to open image from {}. Error: {}",
                            path.to_str().unwrap(),
                            err
                        );
                        tinyfiledialogs::message_box_ok(
                            "Okuna",
                            format!("Failed to open image: {}", err).as_str(),
                            tinyfiledialogs::MessageBoxIcon::Error,
                        );
                        Ok(json_value!([]))
                    }
                }
            }
            None => Ok(json_value!([])),
        }
    }

    fn request_original(
        &mut self,
        args: RequestOriginalArgs,
        runtime_data: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        let img = image::open(&args.identifier).map_err(MethodCallError::from_error)?;
        let mut data = Vec::new();
        let quality = u8::try_from(args.quality).unwrap_or(100);
        img.write_to(&mut data, image::ImageOutputFormat::JPEG(quality))
            .map_err(MethodCallError::from_error)?;
        runtime_data.send_message(
            format!("multi_image_picker/image/{}.original", args.identifier),
            data,
        )?;
        Ok(Value::Boolean(true))
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        runtime_data: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );

        match call.method.as_str() {
            "pickImages" => {
                let args = from_value::<PickImageArgs>(&call.args)?;
                self.pick_images(args)
            }
            "requestOriginal" => {
                let args = from_value(&call.args)?;
                self.request_original(args, runtime_data)
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
