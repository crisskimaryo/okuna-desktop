use flutter_engine::plugins::prelude::*;
use log::debug;

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "plugins.hunghd.vn/image_cropper";

#[derive(Serialize, Deserialize)]
struct CropImageArgs {
    pub source_path: String,
    pub max_width: Option<i32>,
    pub max_height: Option<i32>,
    pub ratio_x: Option<f64>,
    pub ratio_y: Option<f64>,
    pub toolbar_title: String,
    pub toolbar_color: i64,
    pub circle_shape: bool,
}

pub struct ImageCropperPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for ImageCropperPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl ImageCropperPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl Handler {
    fn crop_image(&self, args: CropImageArgs) -> Result<Value, MethodCallError> {
        //TODO: actually implement cropping... or at least respect max_width and max_height
        Ok(Value::String(args.source_path))
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );

        match call.method.as_str() {
            "cropImage" => {
                let args = from_value::<CropImageArgs>(&call.args)?;
                self.crop_image(args)
            }
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
