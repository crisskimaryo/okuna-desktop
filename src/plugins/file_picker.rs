use std::{path::Path, sync::mpsc::SyncSender};

use flutter_engine::plugins::prelude::*;
use log::{debug, info};

const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "file_picker";

pub struct FilePickerPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler {
    default_path: String,
    path_tx: SyncSender<String>,
}

impl Plugin for FilePickerPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl FilePickerPlugin {
    pub fn new(default_path: String, path_tx: SyncSender<String>) -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler {
                default_path,
                path_tx,
            })),
        }
    }
}
impl Handler {
    fn pick_file(&mut self, file_type: FileType, multi: bool) -> Result<Value, MethodCallError> {
        let filter = match file_type {
            FileType::Any => None,
            FileType::Image => Some((
                vec![
                    "*.jpg".into(),
                    "*.png".into(),
                    "*.gif".into(),
                    "*.bmp".into(),
                ],
                "Image files",
            )),
            FileType::Audio => Some((vec!["*.mp3".into()], "Audio files")),
            FileType::Video => Some((
                vec!["*.mp4".into(), "*.mpeg".into(), "*.avi".into()],
                "Video files",
            )),
            FileType::Custom(ext) => Some((vec![ext], "Custom")),
        };
        let filter = if let Some((filter, desc)) = &filter {
            Some((filter.iter().map(|s| s.as_str()).collect::<Vec<_>>(), *desc))
        } else {
            None
        };
        let result = if multi {
            if let Some((filter, desc)) = filter {
                tinyfiledialogs::open_file_dialog_multi(
                    "Okuna",
                    self.default_path.as_str(),
                    Some((&filter[..], desc)),
                )
            } else {
                tinyfiledialogs::open_file_dialog_multi("Okuna", self.default_path.as_str(), None)
            }
        } else {
            if let Some((filter, desc)) = filter {
                tinyfiledialogs::open_file_dialog(
                    "Okuna",
                    self.default_path.as_str(),
                    Some((&filter[..], desc)),
                )
            } else {
                tinyfiledialogs::open_file_dialog("Okuna", self.default_path.as_str(), None)
            }
            .map(|s| vec![s])
        };
        match result {
            Some(files) => {
                info!("Opening files: {}", files.join(","));
                let first_path = files.first().unwrap();
                if let Some(path) = Path::new(first_path).parent() {
                    let path = path.to_str().unwrap();
                    debug!("Setting new default path to {}", path);
                    self.default_path = path.to_owned();
                    self.path_tx.send(self.default_path.clone()).unwrap();
                }

                if multi {
                    Ok(Value::List(
                        files.into_iter().map(|s| Value::String(s)).collect(),
                    ))
                } else {
                    Ok(Value::String(first_path.to_owned()))
                }
            }
            None => Ok(Value::Null),
        }
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );

        let file_type = call.method.as_str();
        let file_type = match file_type {
            "IMAGE" => FileType::Image,
            "VIDEO" => FileType::Video,
            "ANY" => FileType::Any,
            "AUDIO" => FileType::Audio,
            t if t.starts_with("__CUSTOM_") => FileType::Custom(t[9..].to_owned()),
            _ => return Err(MethodCallError::UnspecifiedError),
        };

        let multi = match call.args {
            Value::Boolean(b) => b,
            _ => return Err(MethodCallError::UnspecifiedError),
        };

        self.pick_file(file_type, multi)
    }
}

enum FileType {
    Any,
    Image,
    Audio,
    Video,
    Custom(String),
}
