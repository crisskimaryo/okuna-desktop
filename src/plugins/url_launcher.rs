use flutter_engine::plugins::prelude::*;
use log::debug;

pub const PLUGIN_NAME: &str = module_path!();
pub const CHANNEL_NAME: &str = "plugins.flutter.io/url_launcher";

#[derive(Serialize, Deserialize)]
struct UrlArgs {
    pub url: String,
}

pub struct UrlLauncherPlugin {
    handler: Arc<RwLock<Handler>>,
}

struct Handler;

impl Plugin for UrlLauncherPlugin {
    fn plugin_name() -> &'static str {
        PLUGIN_NAME
    }

    fn init_channels(&mut self, registrar: &mut ChannelRegistrar) {
        let method_handler = Arc::downgrade(&self.handler);
        registrar.register_channel(StandardMethodChannel::new(CHANNEL_NAME, method_handler));
    }
}

impl UrlLauncherPlugin {
    pub fn new() -> Self {
        Self {
            handler: Arc::new(RwLock::new(Handler)),
        }
    }
}

impl MethodCallHandler for Handler {
    fn on_method_call(
        &mut self,
        call: MethodCall,
        _: RuntimeData,
    ) -> Result<Value, MethodCallError> {
        debug!(
            "Got method call {} with args: {}",
            call.method,
            super::debug_print_args(&call.args)
        );
        match call.method.as_str() {
            "canLaunch" => {
                let url = from_value::<UrlArgs>(&call.args)?.url;
                let url = url.as_str();
                Ok(Value::Boolean(
                    url.starts_with("http://") || url.starts_with("https://"),
                ))
            }
            "launch" => {
                let url = from_value::<UrlArgs>(&call.args)?.url;
                match webbrowser::open(url.as_str()) {
                    Ok(_) => Ok(Value::Null),
                    Err(err) => Err(MethodCallError::from_error(err)),
                }
            }
            "closeWebView" => Ok(Value::Null),
            _ => Err(MethodCallError::NotImplemented),
        }
    }
}
