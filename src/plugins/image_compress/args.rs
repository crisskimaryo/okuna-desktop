use std::{fmt, fs::File, io::Error as IOError};

use flutter_engine::codec::Value;
use image::{DynamicImage, ImageError, ImageOutputFormat, ImageResult};
use rexif::ExifResult;
use serde::de::{self, Deserialize, Deserializer, SeqAccess, Visitor};
use serde_repr::{Deserialize_repr, Serialize_repr};

pub enum WriteError {
    IOError(IOError),
    ImageError(ImageError),
}

pub trait CompressArgs {
    fn min_width(&self) -> i32;
    fn min_height(&self) -> i32;
    fn quality(&self) -> i32;
    fn rotate(&self) -> i32;
    fn auto_correction_angle(&self) -> bool;
    fn format(&self) -> CompressFormat;
    fn keep_exif(&self) -> bool;

    fn input_image(&self) -> ImageResult<DynamicImage>;
    fn exif_data(&self) -> ExifResult;

    fn write(&self, img: &DynamicImage) -> Result<Value, WriteError>;

    fn output_format(&self) -> ImageOutputFormat {
        match self.format() {
            CompressFormat::PNG => ImageOutputFormat::PNG,
            CompressFormat::JPEG => ImageOutputFormat::JPEG(self.quality() as u8),
        }
    }
}

#[derive(Serialize_repr, Deserialize_repr, Copy, Clone)]
#[repr(i32)]
pub enum CompressFormat {
    JPEG = 0,
    PNG = 1,
}

pub struct CompressListArgs {
    data: Vec<u8>,
    min_width: i32,
    min_height: i32,
    quality: i32,
    rotate: i32,
    auto_correction_angle: bool,
    format: CompressFormat,
    keep_exif: bool,
}

pub struct CompressFileArgs {
    file: String,
    min_width: i32,
    min_height: i32,
    quality: i32,
    rotate: i32,
    auto_correction_angle: bool,
    format: CompressFormat,
    keep_exif: bool,
}

pub struct CompressFileToFileArgs {
    file: String,
    min_width: i32,
    min_height: i32,
    quality: i32,
    target_path: String,
    rotate: i32,
    auto_correction_angle: bool,
    format: CompressFormat,
    keep_exif: bool,
}

impl CompressArgs for CompressListArgs {
    fn min_width(&self) -> i32 {
        self.min_width
    }
    fn min_height(&self) -> i32 {
        self.min_height
    }
    fn quality(&self) -> i32 {
        self.quality
    }
    fn rotate(&self) -> i32 {
        self.rotate
    }
    fn auto_correction_angle(&self) -> bool {
        self.auto_correction_angle
    }
    fn format(&self) -> CompressFormat {
        self.format
    }
    fn keep_exif(&self) -> bool {
        self.keep_exif
    }

    fn input_image(&self) -> ImageResult<DynamicImage> {
        image::load_from_memory(&self.data)
    }
    fn exif_data(&self) -> ExifResult {
        rexif::parse_buffer(&self.data)
    }

    fn write(&self, img: &DynamicImage) -> Result<Value, WriteError> {
        let mut data = Vec::new();
        img.write_to(&mut data, self.output_format())
            .map_err(WriteError::ImageError)?;
        Ok(Value::U8List(data))
    }
}

impl CompressArgs for CompressFileArgs {
    fn min_width(&self) -> i32 {
        self.min_width
    }
    fn min_height(&self) -> i32 {
        self.min_height
    }
    fn quality(&self) -> i32 {
        self.quality
    }
    fn rotate(&self) -> i32 {
        self.rotate
    }
    fn auto_correction_angle(&self) -> bool {
        self.auto_correction_angle
    }
    fn format(&self) -> CompressFormat {
        self.format
    }
    fn keep_exif(&self) -> bool {
        self.keep_exif
    }

    fn input_image(&self) -> ImageResult<DynamicImage> {
        image::open(&self.file)
    }
    fn exif_data(&self) -> ExifResult {
        rexif::parse_file(&self.file)
    }

    fn write(&self, img: &DynamicImage) -> Result<Value, WriteError> {
        let mut data = Vec::new();
        img.write_to(&mut data, self.output_format())
            .map_err(WriteError::ImageError)?;
        Ok(Value::U8List(data))
    }
}

impl CompressArgs for CompressFileToFileArgs {
    fn min_width(&self) -> i32 {
        self.min_width
    }
    fn min_height(&self) -> i32 {
        self.min_height
    }
    fn quality(&self) -> i32 {
        self.quality
    }
    fn rotate(&self) -> i32 {
        self.rotate
    }
    fn auto_correction_angle(&self) -> bool {
        self.auto_correction_angle
    }
    fn format(&self) -> CompressFormat {
        self.format
    }
    fn keep_exif(&self) -> bool {
        self.keep_exif
    }

    fn input_image(&self) -> ImageResult<DynamicImage> {
        image::open(&self.file)
    }
    fn exif_data(&self) -> ExifResult {
        rexif::parse_file(&self.file)
    }

    fn write(&self, img: &DynamicImage) -> Result<Value, WriteError> {
        let mut file = File::create(&self.target_path).map_err(WriteError::IOError)?;
        img.write_to(&mut file, self.output_format())
            .map_err(WriteError::ImageError)?;
        Ok(Value::String(self.target_path.clone()))
    }
}

impl<'de> Deserialize<'de> for CompressListArgs {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct CompressListArgsVisitor;

        impl<'de> Visitor<'de> for CompressListArgsVisitor {
            type Value = CompressListArgs;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct CompressListArgs")
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<Self::Value, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let data = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                let min_width = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(1, &self))?;
                let min_height = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(2, &self))?;
                let quality = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(3, &self))?;
                let rotate = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(4, &self))?;
                let auto_correction_angle = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(5, &self))?;
                let format = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(6, &self))?;
                let keep_exif = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(7, &self))?;

                Ok(CompressListArgs {
                    data,
                    min_width,
                    min_height,
                    quality,
                    rotate,
                    auto_correction_angle,
                    format,
                    keep_exif,
                })
            }
        }

        deserializer.deserialize_seq(CompressListArgsVisitor)
    }
}

impl<'de> Deserialize<'de> for CompressFileArgs {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct CompressFileArgsVisitor;

        impl<'de> Visitor<'de> for CompressFileArgsVisitor {
            type Value = CompressFileArgs;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct CompressFileArgs")
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<Self::Value, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let file = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                let min_width = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(1, &self))?;
                let min_height = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(2, &self))?;
                let quality = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(3, &self))?;
                let rotate = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(4, &self))?;
                let auto_correction_angle = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(5, &self))?;
                let format = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(6, &self))?;
                let keep_exif = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(7, &self))?;

                Ok(CompressFileArgs {
                    file,
                    min_width,
                    min_height,
                    quality,
                    rotate,
                    auto_correction_angle,
                    format,
                    keep_exif,
                })
            }
        }

        deserializer.deserialize_seq(CompressFileArgsVisitor)
    }
}

impl<'de> Deserialize<'de> for CompressFileToFileArgs {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct CompressFileToFileArgsVisitor;

        impl<'de> Visitor<'de> for CompressFileToFileArgsVisitor {
            type Value = CompressFileToFileArgs;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("struct CompressFileToFileArgs")
            }

            fn visit_seq<V>(self, mut seq: V) -> Result<Self::Value, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let file = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                let min_width = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(1, &self))?;
                let min_height = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(2, &self))?;
                let quality = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(3, &self))?;
                let target_path = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(4, &self))?;
                let rotate = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(5, &self))?;
                let auto_correction_angle = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(6, &self))?;
                let format = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(7, &self))?;
                let keep_exif = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(8, &self))?;

                Ok(CompressFileToFileArgs {
                    file,
                    min_width,
                    min_height,
                    quality,
                    target_path,
                    rotate,
                    auto_correction_angle,
                    format,
                    keep_exif,
                })
            }
        }

        deserializer.deserialize_seq(CompressFileToFileArgsVisitor)
    }
}
