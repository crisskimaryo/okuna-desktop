#![cfg_attr(all(windows, not(debug_assertions)), windows_subsystem = "windows")]

use std::panic::PanicInfo;

use clap::{App, Arg};
use flutter_engine::{codec::Value, DesktopWindowState};
use locale_config::{LanguageRange, Locale};
use log::{debug, error, info};

use crate::plugins::{FlutterSecureStoragePlugin, SharedPreferencesPlugin};
use crate::util::paths;

mod logging;
mod plugins;
mod settings;
mod util;

const STORAGE_KEY_WINDOW_WIDTH: &str = "desktop.window_width";
const STORAGE_KEY_WINDOW_HEIGHT: &str = "desktop.window_height";
const PREF_KEY_IMAGE_PICKER_DIR: &str = "desktop.image-picker.last-dir";

#[cfg(not(target_os = "macos"))]
const FUNCTION_MODIFIER_KEY: glfw::Modifiers = glfw::Modifiers::Control;
#[cfg(target_os = "macos")]
const FUNCTION_MODIFIER_KEY: glfw::Modifiers = glfw::Modifiers::Super;

type PerFrameCallback = dyn FnOnce(&mut DesktopWindowState) + Send;

fn main() {
    let matches = App::new("okuna-desktop")
        .version(version::version!())
        .arg(
            Arg::with_name("v")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increase verbosity. May be specified multiple times."),
        )
        .get_matches();

    let (mut verbosity, log_to_file) = if cfg!(debug_assertions) {
        (2, false)
    } else {
        (1, true)
    };
    verbosity += matches.occurrences_of("v") as u8;
    logging::setup_logging(verbosity, log_to_file).expect("Failed to setup logging");

    info!("Starting okuna-desktop v{}", version::version!());
    std::panic::set_hook(Box::new(panic_handler));

    util::path_upgrade::upgrade();

    debug!("Loading settings");
    settings::get_settings();

    debug!("Configuring locale");
    let mut locale = Locale::global_default();
    #[cfg(target_os = "macos")]
    {
        add_macos_locale(&mut locale);
    }
    let mut has_lang = false;
    for lang in locale.tags_for("") {
        if lang != LanguageRange::invariant() {
            has_lang = true;
            break;
        }
    }
    if !has_lang {
        info!("Adding fallback language");
        locale.add(&LanguageRange::new("en-GB").expect("Failed to parse fallback language"));
    }
    Locale::set_global_default(locale);

    let (flutter_assets_path, icu_data_path) = paths::get_flutter_paths();
    let assets_path = paths::get_assets_dir();

    let storage = FlutterSecureStoragePlugin::new();
    let width = storage
        .read_value(STORAGE_KEY_WINDOW_WIDTH)
        .and_then(|s| s.parse().ok())
        .and_then(|w| if w == 0 { None } else { Some(w) })
        .unwrap_or(500);
    let height = storage
        .read_value(STORAGE_KEY_WINDOW_HEIGHT)
        .and_then(|s| s.parse().ok())
        .and_then(|h| if h == 0 { None } else { Some(h) })
        .unwrap_or(900);

    let shared_prefs = SharedPreferencesPlugin::new();
    let prefs = shared_prefs.get_all();
    let image_picker_dir = prefs
        .get(PREF_KEY_IMAGE_PICKER_DIR)
        .and_then(|v| {
            if let Value::String(s) = v {
                Some(s.as_str())
            } else {
                None
            }
        })
        .map_or_else(
            || dirs::home_dir().map_or("".into(), |s| s.to_str().unwrap_or("").to_owned()),
            ToOwned::to_owned,
        );

    let icons = if let image::DynamicImage::ImageRgba8(image) =
        image::open(assets_path.join("okuna-o-logo_transparent_256.png"))
            .expect("Failed to open logo image")
    {
        Some(vec![
            image::imageops::resize(&image, 16, 16, image::FilterType::Lanczos3),
            image::imageops::resize(&image, 32, 32, image::FilterType::Lanczos3),
            image::imageops::resize(&image, 48, 48, image::FilterType::Lanczos3),
        ])
    } else {
        None
    };

    ffmpeg::init().unwrap();

    let (image_picker_path_tx, image_picker_path_rx) = std::sync::mpsc::sync_channel(1024);
    let (frame_fn_tx, frame_fn_rx) = std::sync::mpsc::sync_channel(1024);

    debug!("Loading flutter engine");
    let mut engine = flutter_engine::init().expect("Failed to initialize flutter engine");
    engine
        .create_window(
            &flutter_engine::WindowArgs {
                width,
                height,
                title: &"Okuna",
                mode: flutter_engine::WindowMode::Windowed,
                bg_color: (255, 255, 255),
            },
            flutter_assets_path.to_string_lossy().to_string(),
            icu_data_path.to_string_lossy().to_string(),
            vec![],
        )
        .expect("Failed to create window");
    debug!("Initializing");
    engine.init_with_window_state(|window_state| {
        if let Some(icons) = icons {
            window_state.window().set_icon(icons);
        }

        window_state
            .plugin_registrar
            .add_plugin(plugins::ConnectivityPlugin::new())
            .add_plugin(plugins::ErrorReportingPlugin::new())
            .add_plugin(plugins::FFmpegPlugin::new())
            .add_plugin(plugins::FilePickerPlugin::new(
                image_picker_dir.clone(),
                image_picker_path_tx.clone(),
            ))
            .add_plugin(storage)
            .add_plugin(plugins::GetVersionPlugin::new())
            .add_plugin(plugins::ImageCompressPlugin::new())
            .add_plugin(plugins::ImageConverterPlugin::new())
            .add_plugin(plugins::ImageCropperPlugin::new())
            .add_plugin(plugins::ImagePickerPlugin::new(
                image_picker_dir.clone(),
                image_picker_path_tx.clone(),
            ))
            .add_plugin(plugins::MultiImagePickerPlugin::new(
                image_picker_dir,
                image_picker_path_tx,
            ))
            .add_plugin(plugins::PackageInfoPlugin::new())
            .add_plugin(plugins::PathProviderPlugin::new())
            .add_plugin(plugins::ProxySettingsPlugin::new())
            .add_plugin(shared_prefs)
            .add_plugin(plugins::SqflitePlugin::new())
            .add_plugin(plugins::UrlLauncherPlugin::new())
            .add_plugin(plugins::VideoPlayerPlugin::new(frame_fn_tx))
            .add_plugin(plugins::VideoThumbnailPlugin::new());
    });
    debug!("Running app");
    engine.run_window_loop(
        Some(&mut glfw_event_handler),
        Some(&mut move |window_state| {
            if let Ok(new_path) = image_picker_path_rx.try_recv() {
                window_state.plugin_registrar.with_plugin_mut(
                    |shared_prefs: &mut SharedPreferencesPlugin| {
                        shared_prefs
                            .set(PREF_KEY_IMAGE_PICKER_DIR.into(), Value::String(new_path))
                            .unwrap();
                    },
                );
            }
            while let Ok(frame_fn) = frame_fn_rx.try_recv() {
                frame_fn(window_state);
            }
        }),
    );
    info!("Shutdown");
}

fn glfw_event_handler(window_state: &mut DesktopWindowState, event: glfw::WindowEvent) -> bool {
    match event {
        glfw::WindowEvent::Size(width, height) => {
            if width > 0 && height > 0 {
                window_state.plugin_registrar.with_plugin_mut(
                    |p: &mut FlutterSecureStoragePlugin| {
                        p.write_value(String::from(STORAGE_KEY_WINDOW_WIDTH), format!("{}", width));
                        p.write_value(
                            String::from(STORAGE_KEY_WINDOW_HEIGHT),
                            format!("{}", height),
                        );
                    },
                );
            }
            true
        }
        // Only send the raw key event for Ctrl/Command+V, the flutter engine will paste text itself
        glfw::WindowEvent::Key(glfw::Key::V, scancode, glfw::Action::Press, modifiers)
            if modifiers.contains(FUNCTION_MODIFIER_KEY) =>
        {
            window_state.plugin_registrar.with_plugin_mut(
                |keyevent: &mut flutter_engine::plugins::KeyEventPlugin| {
                    keyevent.key_action(false, glfw::Key::V, scancode, modifiers);
                },
            );
            false
        }
        _ => true,
    }
}

fn panic_handler(info: &PanicInfo) {
    let location = info.location().unwrap(); // The current implementation always returns Some
    let msg = match info.payload().downcast_ref::<&'static str>() {
        Some(s) => *s,
        None => match info.payload().downcast_ref::<String>() {
            Some(s) => &s[..],
            None => "Box<Any>",
        },
    };
    let thread = std::thread::current();
    let name = thread.name().unwrap_or("<unnamed>");

    error!("thread '{}' panicked at '{}', {}", name, msg, location);

    let mut frame_counter = 0;

    backtrace::trace(|frame| {
        let ip = frame.ip();
        backtrace::resolve(ip, |symbol| {
            let name = symbol
                .name()
                .map(|name| name.to_string())
                .unwrap_or_else(|| "<unnamed>".into());
            let addr = symbol.addr().unwrap_or_else(std::ptr::null_mut);
            let filename = symbol.filename().map_or_else(
                || "<unknown source>".into(),
                |path| path.to_string_lossy().into_owned(),
            );
            let line_number = symbol.lineno().unwrap_or(0);

            error!(
                "#{}: {:?}:{} at {}:{}",
                frame_counter, addr, name, filename, line_number
            );
        });
        frame_counter += 1;
        true
    });
}

#[cfg(target_os = "macos")]
fn add_macos_locale(locale: &mut Locale) {
    use objc::{class, msg_send, runtime::Object, sel, sel_impl};
    use objc_foundation::{INSString, NSString};

    unsafe {
        let nslocale = class!(NSLocale);
        let current_locale: *mut Object = msg_send![nslocale, currentLocale];
        let locale_identifier: *const NSString = msg_send![current_locale, localeIdentifier];
        let locale_identifier = locale_identifier.as_ref().unwrap();
        locale.add(&LanguageRange::from_unix(locale_identifier.as_str()).unwrap());
    }
}
