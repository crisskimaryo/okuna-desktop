use std::io::Error as IOError;
use std::{fmt, fs, path::PathBuf};

use log::{debug, error, info};

pub fn upgrade() {
    upgrade_single_path(dirs::data_dir(), "openbook", "openspace");
    upgrade_single_path(dirs::cache_dir(), "openbook", "openspace");
    upgrade_single_path(dirs::data_dir(), "openspace", "okuna");
    upgrade_single_path(dirs::cache_dir(), "openspace", "okuna");
}

fn upgrade_single_path(path: Option<PathBuf>, old_path: &str, new_path: &str) {
    if let Some(dir) = path {
        let old_dir = dir.join(old_path);
        let new_dir = dir.join(new_path);
        if !old_dir.exists() {
            return;
        }
        info!(
            "Upgrading path from \"{}\" to \"{}\"",
            old_dir.to_string_lossy(),
            new_dir.to_string_lossy()
        );

        if !new_dir.exists() {
            match fs::rename(&old_dir, &new_dir) {
                Ok(()) => {
                    info!("Renamed directory");
                }
                Err(e) => {
                    error!("Failed to rename. Error: {}", e);
                }
            }
        } else if !new_dir.is_dir() {
            error!("New path exists and is not a directory");
        } else if let Err(e) = copy_dir(&old_dir, &new_dir) {
            error!("Failed to copy files. Error: {}", e);
        }
    }
}

#[derive(Debug)]
enum CopyError {
    DestinationFileExists(PathBuf),
    DestinationIsNotDir(PathBuf),
    CreateDirectoryFailed(IOError),
    CopyFileFailed(IOError),
    RemoveFailed(IOError),
    ReadDirFailed(IOError),
    SubCopyFailed(Vec<(PathBuf, CopyError)>),
}

impl std::error::Error for CopyError {}

impl fmt::Display for CopyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            CopyError::DestinationFileExists(path) => write!(
                f,
                "Destination file \"{}\" already exists",
                path.to_string_lossy()
            ),
            CopyError::DestinationIsNotDir(path) => write!(
                f,
                "Destination \"{}\" already exists and is not a directory",
                path.to_string_lossy()
            ),
            CopyError::CopyFileFailed(e) => write!(f, "Failed to copy file. Error: {}", e),
            CopyError::RemoveFailed(e) => write!(f, "Failed to remove old entry. Error: {}", e),
            CopyError::CreateDirectoryFailed(e) => {
                write!(f, "Failed to create new directory. Error: {}", e)
            }
            CopyError::ReadDirFailed(e) => write!(f, "Failed to read directory. Error: {}", e),
            CopyError::SubCopyFailed(sub) => {
                writeln!(f, "Failed to copy entries:")?;
                for (path, e) in sub {
                    writeln!(
                        f,
                        "Error when copying \"{}\". Error: {}",
                        path.to_string_lossy(),
                        e
                    )?;
                }
                Ok(())
            }
        }
    }
}

fn copy_files(old_dir: &PathBuf, new_dir: &PathBuf) -> Result<(), CopyError> {
    let mut sub_error = Vec::new();
    for entry in fs::read_dir(old_dir).map_err(CopyError::ReadDirFailed)? {
        let entry = match entry {
            Ok(entry) => entry,
            Err(e) => {
                sub_error.push((old_dir.to_path_buf(), CopyError::ReadDirFailed(e)));
                continue;
            }
        };
        let path = entry.path();
        let new_path = new_dir.join(path.file_name().unwrap());
        if path.is_dir() {
            if let Err(e) = copy_dir(&path, &new_path) {
                sub_error.push((path, e));
            }
        } else if path.is_file() {
            if let Err(e) = copy_file(&path, &new_path) {
                sub_error.push((path, e));
            }
        }
    }
    if !sub_error.is_empty() {
        Err(CopyError::SubCopyFailed(sub_error))
    } else {
        Ok(())
    }
}

fn copy_dir(old_path: &PathBuf, new_path: &PathBuf) -> Result<(), CopyError> {
    if !new_path.exists() {
        fs::create_dir(&new_path).map_err(CopyError::CreateDirectoryFailed)?;
    } else if !new_path.is_dir() {
        return Err(CopyError::DestinationIsNotDir(new_path.to_path_buf()));
    }
    debug!(
        "Copying files from \"{}\" to \"{}\"",
        old_path.to_string_lossy(),
        new_path.to_string_lossy()
    );
    match copy_files(&old_path, &new_path) {
        Err(CopyError::SubCopyFailed(sub)) => Err(CopyError::SubCopyFailed(sub)),
        Err(e) => Err(CopyError::SubCopyFailed(vec![(old_path.to_path_buf(), e)])),
        Ok(()) => fs::remove_dir(&old_path).map_err(CopyError::RemoveFailed),
    }
}

fn copy_file(old_path: &PathBuf, new_path: &PathBuf) -> Result<(), CopyError> {
    if new_path.exists() {
        return Err(CopyError::DestinationFileExists(new_path.to_path_buf()));
    }
    fs::copy(&old_path, &new_path).map_err(CopyError::CopyFileFailed)?;
    debug!(
        "Copied \"{}\" to \"{}\"",
        old_path.to_string_lossy(),
        new_path.to_string_lossy()
    );
    fs::remove_file(&old_path).map_err(CopyError::RemoveFailed)
}
