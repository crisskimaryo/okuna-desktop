# Changelog

## vnext

* Fix double paste bug

## v0.0.55-1

* Add support for connectivity plugin
  * This fixes links previews and allows video autoplay
* Fix infinite video loading bug
* Add app hotfixes from upstream

## v0.0.55

* Fix multiple FFmpeg issues that prevented video thumbnails from showing up
* Add AppImage for easy install on Linux
* Truncate latest.log before writing
* Lower versions of required dependencies on Linux
* Display version number in-app
* Restore full functionality of about page

## v0.0.52-hotfix.1

* Fix debian packaging

## v0.0.52

* Upgrade to app version 0.0.52
* Add support for video playback
    * Audio is currently not supported
* Add support for file picker plugin
* Add support for image compression
* Add support for video compression
* Add support for video thumbnails

## v0.0.51-1

* Add support for proxy auto configuration scripts
* Add app settings specific to Okuna Desktop
    * Proxy auto configuration has to be enabled manually
    * Default proxy can be set, overriding OS proxy

## v0.0.51

* Automatically build packages for Archlinux and Debian
* Upgrade to app version 0.0.51

## v0.0.50-1

* Fix errors when proxy configuration fails
* Implement log file rotation and log compression
* Write more useful logs

## v0.0.50

* Fix being unable to upload images when posting something
* Implement HTTP proxy detection
* Upgrade to app version 0.0.50

## v0.0.49-2

* Fix localisation errors on macOS

## v0.0.49-1

* Fix keyboard events spamming the log

## v0.0.49

* Renamed from openspace to okuna
* Add error message when opening an image fails
* Upgrade to app version 0.0.49

## v0.0.45

* Upgrade to app version 0.0.45

## v0.0.44-1

* Changed window title from Openbook to Openspace
* Added metadata to Windows exe

## v0.0.44

* Fixed an issue where it was impossible to write in text fields
* Upgraded libraries
* Upgrade to app version 0.0.44
* Add preliminary support for Multi Image Picker plugin
* Change emoji font
* Use different emoji fonts depending on OS
* Renamed from openbook to openspace

## v0.0.43-2

* Fixed section links in community guidelines opening in the browser
* Fixed crash when uploading header image
* Fixed zero size window leading to crash on startup
* Fixed not deleting data making it impossible to log out
* Added icon to Windows exe
* Store last path in image picker

## v0.0.43-1

* Fixed image uploading
* Upgraded libraries
* Fixed threading issues

## v0.0.43

* Increased scrolling speed
* Improved content scaling
* Implemented better error logging
* Internal code improvements
* Upgrade to app version 0.0.43

## v0.0.41

* Upgrade to app version 0.0.41

## v0.0.34-6-rc3

* Fixed a memory leak when not responding to flutter platform messages
* Moved image picker dialog to new thread
* Changed image picker to copy and resize the selected image
* Write rust panics to log instead of stderr

## v0.0.34-6-rc2

* Added mouse scrolling

## v0.0.34-6-rc1

* Nothing because I messed the CI up

## v0.0.34-5

* Fixed a crash when pasting an image into the app
* Fixed macOS CI build

## v0.0.34-4

* Changed log file path on Windows and Linux to support installation
* Added command line parameters for version and increased log level
* Added Option+Left/Right keys on macOS to jump an entire word in the text

## v0.0.34-3

* Added a window icon
* Added icon to macOS bundle
* Reduced log spam
* Code improvements

## v0.0.34-2

* Fixed macOS CI build

## v0.0.34-1

* Added caching to CI builds
* Fixed macOS CI build
* Changed log file path on macOS

## v0.0.34

* Upgraded app version to 0.0.34

## v0.0.29-1

* Added macOS build

## v0.0.29

* Upgraded app version to 0.0.29
* Window size is now saved and restored

## v0.0.22-1

* Emoji will render system-independently

## v0.0.22

* Upgraded app version to 0.0.22
* Ctrl+Arrow now works in text fields
* Home and End keys immediately change cursor position
* Mouse Button 4 is handled as a back button

## v0.0.21-6

* Fixed missing fonts

## v0.0.21-5

* Implemented `image_picker` plugin
* Improved scaling
* Added support for repeating key presses

## v0.0.21-4

* Change environment for CI

## v0.0.21-3

* Implemented `url_launcher` and `sqflite` plugins
* Added support for clicking on links
* Added support for header images

## v0.0.21-2

* Add CI builds

## v0.0.21

* Upgraded app version to 0.0.21

## v0.0.20

* Initial release
