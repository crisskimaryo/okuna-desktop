#!/usr/bin/env bash

root_dir=$(pwd)
cd "$(dirname "$0")"
source ../common.sh

pkgname="okuna-desktop-${build}"
pkgrel=1

echo "building package ${pkgname} ${version}-${pkgrel}"

sed -i "s/^pkgname=.*\$/pkgname=\"\${_pkgname}-${build}\"/" PKGBUILD
sed -i "s/^pkgver=.*\$/pkgver=$version/" PKGBUILD
sed -i "s/^pkgrel=.*\$/pkgrel=$pkgrel/" PKGBUILD
sed -i "s/\${job_id}/$job_id/g" PKGBUILD
sed -i "s/#sha256/$(makepkg --geninteg)/" PKGBUILD
makepkg --printsrcinfo > .SRCINFO
source PKGBUILD && sudo pacman -S --noconfirm --needed --asdeps "${makedepends[@]}"
makepkg --nodeps
