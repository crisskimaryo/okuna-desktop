#!/usr/bin/env bash

root_dir=$(pwd)
cd "$(dirname "$0")"
source ../common.sh

pkgname="okuna-desktop${build#bin}"
pkgrel=1

echo "building package ${pkgname} ${version}-${pkgrel}"

srczip="${pkgname}-${version}.zip"
srcdir="${pkgname}-${version}"
curl -SsLo "${srczip}" "https://gitlab.com/999eagle/okuna-desktop/-/jobs/${job_id}/artifacts/download"
unzip "${srczip}"
mv "okuna-desktop" "${srcdir}"
tar caf "${pkgname}_${version}.orig.tar.gz" "${srcdir}"
rm "${srczip}"
mkdir "${srcdir}/debian"

# Debian files
for i in 'compat' 'control' 'rules' 'copyright'; do
	cp $i "${srcdir}/debian/$i"
done

if [[ "${pkgname}" != 'okuna-desktop' ]]; then
	sed -i "s/okuna-desktop/${pkgname}/g" "${srcdir}/debian/control"
fi

# License
for line in $(cat "${root_dir}/LICENSE"); do
	if [[ -z "${line}" ]]; then
		line="."
	fi
	echo " $line" >> "${srcdir}/debian/copyright"
done

# Changelog
cat << EOF > "${srcdir}/debian/changelog"
${pkgname} (${version}-${pkgrel}) stable; urgency=medium
EOF
./changelog.awk "${root_dir}/CHANGELOG.md" >> "${srcdir}/debian/changelog"
cat << EOF >> "${srcdir}/debian/changelog"
 -- Sophie Tauchert <sophie@999eagle.moe>  $(date +"%a, %d %b %Y %T %z")
EOF

cd "${srcdir}"
debuild -us -uc
