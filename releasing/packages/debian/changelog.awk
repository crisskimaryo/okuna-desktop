#!/usr/bin/awk -f
(state == 0) && /^## v[0-9]+\./ { state=1; next }
(state == 1) && /^## v[0-9]+\./ { state=2; next }
state == 1 { print "  " $0 }
