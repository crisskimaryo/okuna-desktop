#!/usr/bin/env bash

pkgname="okuna-desktop"
pkgdir="$(pwd)/debian/${pkgname}"

echo "Enabling extended globbing"
shopt -s extglob

echo "Installing files into ${pkgdir}"

echo "Copying files to opt/${pkgname}"
# Main files
mkdir -p "${pkgdir}/opt/${pkgname}"
cp -a !(debian) "${pkgdir}/opt/${pkgname}"

echo "Creating symlink in usr/bin"
# Binary symlink
mkdir -p "${pkgdir}/usr/bin"
ln -s "/opt/${pkgname}/${pkgname}.sh" "${pkgdir}/usr/bin/${pkgname}"

echo "Copying desktop entry"
# Desktop entry
mkdir -p "${pkgdir}/usr/share/applications"
mv "${pkgdir}/opt/${pkgname}/${pkgname}.desktop" "${pkgdir}/usr/share/applications"

echo "Copying icons"
# Icons
for size in 32 64 256; do
	path="${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
	mkdir -p "${path}"
	ln -s "/opt/${pkgname}/assets/okuna-o-logo_transparent_${size}.png" "${path}/${pkgname}.png"
done
