#!/usr/bin/env bash

BASE_NAME="okuna-o-logo_transparent"

cd assets
mkdir -p ${BASE_NAME}.iconset

for size in "32" "64" "256"; do
	cp ${BASE_NAME}_${size}.png ${BASE_NAME}.iconset/icon_${size}x${size}.png
done

iconutil -c icns -o OkunaDesktop.icns ${BASE_NAME}.iconset
