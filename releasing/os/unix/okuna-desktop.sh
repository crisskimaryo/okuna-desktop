#!/usr/bin/env bash

ABS_PATH="$(readlink "$0")"
if [[ -z "$ABS_PATH" ]]; then ABS_PATH="$0"; fi

if which osascript >/dev/null; then
  export LANG="$(osascript -e 'user locale of (get system info)')"
fi

cd "$(dirname "$ABS_PATH")"
./okuna-desktop "$@"
